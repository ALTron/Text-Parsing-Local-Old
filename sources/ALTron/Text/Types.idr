module ALTron.Text.Types

-- import ALTron.Base.Char.Unicode

record Fraction where
    constructor MkFraction
    numerator, denominator : Integer

record Scientific where
    constructor MkScientific
    mantissa, power : Integer

--     МыслеОбраз,
data DataPointer = Cursor | Symbol | Line | Indent

data PartText : Type where
  Word, Sentence: PartText

data PartPaperBook = Page

data PartBook  : Type where
    Paragraph, Section, SubSection, Indroduction, Chapter, Part, Volume : PartBook
--    Paragraph?
{-
getMantissa : Double -> Integer
getMantissa x = if x `mod` 10 == 0 then
FromDouble Fraction where
  fromDouble x = MkFraction (the Integer (cast x)) 1

Show Fraction where
  show x = show (numerator x) ++ "/" ++ show (denominator x)

FromDouble Scientific where
  fromDouble x = MkFraction (the Integer (cast x)) 1

Show Scientific where
  show x = show (numerator x) ++ "/" ++ show (denominator x)

 Далее не нужный код, можно удалить
||| The Parser data type - takes a string argument 
||| and produces a result and the unconsumed string
public export
data Parser : Type -> Type where
  MkParser : (String -> List (a, String)) -> Parser a
  
data Key1 : Type -> Type where
  CKey1 : a -> Key1 a

data Alpha1 : Char -> Type -> Type where
  CAlpha1 : x -> (Alpha1 (x : isAlpha a = True))
  CAlpha1 : { x : Char | isAlpha x } -> Alpha1 Char
  CAlpha1 : (x : isAlpha a = True) -> Alpha1 Char - компилируется, но скорее всего неправильно
{ n : Int | n > a & n > b }


public export
AlphaChar : Type
AlphaChar = (c : Char ** isAlpha c = True)

public export
data Alpha1 : Type -> Type where
  CAlpha1 : AlphaChar -> Alpha1 AlphaChar

public export
data Alpha2 : Type -> Type where
  CAlpha2 : AlphaChar -> Alpha2 Char
  
public export
data Alpha3 : Type -> Type where
  CAlpha3 : (x : Char) -> Alpha3 (x : Char ** isAlpha x = True)

public export
data Range1 : Type -> Type where
  CRange1 : (Ord a, Num a, Show a) => (x : a) -> Range1 (x ** x < 3 = True)


data Range : (Ord a, Num a) => (x : a) -> x < 3 = True -> Type where
  CRange : (Ord a, Num a) => (x : a) -> x < 3 = True -> Range x

public export
data Alpha2 : Type -> Type where
  CAlpha2 : (x : Char) -> { x : Char | isAlpha x } -> Alpha2 Char
-}
{-
public export
data Key2 : Type where
  CKey2 : String -> Key2

public export
data Value2 : Type -> Type where
  CValue2 :(Show a) => a -> Value2 a

public export
data Pair1 : Type -> Type where
  CPair1 : (Show a) => Key2 -> Value2 a -> Pair1 a

public export
data KeyS = CKey String
-- Key a = CKey a
data Value a = CValue a
data Pair a = CPair (KeyS) (Value a)
data Lexem a = CLexem (Pair a)
data Lexem1 : Type -> Type where
  CLexem1 : a -> Pair a -> Lexem1 a

public export
Show Key2 where
    show (CKey2 x) = x

public export
Show (Value2 a) where
    show (CValue2 x) = show x

public export
Show (Pair1 a) where
    show (CPair1 x y) = show x ++ " : " ++ show y

public export
Show KeyS where
    show (CKey x) = x

public export
Show (Alpha1 AlphaChar) where
    show (CAlpha1 (DPair x y)) = show x ++ " pred " ++ show y
    
public export
FromChar (AlphaChar : DPair Char _) where
    fromChar (DPair x _) = x 

public export
FromChar (Alpha2 Char) where
  fromChar x = (CAlpha2 (x ** ?asd))
-}
