module Main

-- import Types
-- import ALTron.Base.Char.Unicode
import ALTron.Text.Types
-- import ALTron.System.File
-- import ALTron.Dedtysflap.Local.Chez
-- import ALTron.Precalcs.Types
-- import Control.App.FileIO
-- import ALTron.Adic.Parser.Local
import Data.String
import Data.String.Extra
import Data.SnocList
import Data.Vect
import Debug.Trace
import System.File

getSum : String -> Double
getSum x = cast x + 3.2

greet : IO ()
greet = do
    putStrLn "comission"
    x <- getLine
    putStrLn ("Hello " ++ show (getSum x))

-- FileError
{-
readDic : (h : File) -> IO (Either String (List (Pair1 String))) -- len : Nat ** Vect len String))
readDic h = readDicHelper h []
  where
    readDicHelper : (h : File) -> (acc : List (Pair1 String)) -> IO (Either String (List (Pair1 String)))
    readDicHelper h acc = do
        if !(fEOF h)
          then pure $ Right acc
          else do
            Right s1 <- fGetLine h
                | Left err => do
    --                printLn "Ошибка чтения файла"
                    pure $ Left "тест1"
            putStrLn $ "s1 " ++ show s1
            if (s1 == "") -- || (s1 == "\n")
                then
                    readDicHelper h acc
                else case eval s1 of 
                    Just p@(CPair1 x v@(CValue2 y)) => do
                        trace (s1 ++ " pair :" ++ show p) readDicHelper h (acc ++ [p])
                    Nothing => do
        --                printLn "Invalid input"
                        pure $ Left "test2"
                        -}
-- replaceWords : String -> List (Pair1 String) -> String
-- replaceWords xs ys = 
{-
findByKey : String -> List (Pair1 String) -> Maybe (Pair1 String)
findByKey _ [] = Nothing
findByKey xs ((p@(CPair1 (CKey2 x) (CValue2 y))) :: ys) = 
     if xs == x then Just p else
        findByKey xs ys
        
getWord : String -> List (Pair1 String) -> String
getWord xs ys = let res = findByKey xs ys
    in case res of
        Just (CPair1 (CKey2 x) (CValue2 y)) => y
        Nothing => xs
-}
getFst : (x: a ** _) -> a
getFst (x ** _) = x

main : IO ()
main = do
    let
{-      d : Alpha3 (x : Char ** isAlpha x = True)
d = (CAlpha3 'A' ** Refl) -}
--      x2 : (Ord a, Num a) => (x : a) -> x < 3 = True -> Range x
      x4 : Fin 3
      x4 = 1
--      d1 :(x : Char ** isAlpha x = True)
--      d1 = the 'x' (x : Char ** isAlpha x = True)
{-    Right f1 <- openFile "test.adic" Read
        | Left err => printLn err
    Right s1 <- fGetLine f
        | Left err => printLn err
    printLn $ dropLast 1 s1
    case eval s1 of
        Just (CPair1 x v@(CValue2 y)) => do
            putStrLn (show x)
            putStrLn "Value: "
            putStrLn y
        Nothing => putStrLn "Invalid input" {--}

    Right s2 <- fGetLine f
        | Left err => printLn err
    putStrLn s2
    case eval s2 of 
        Just (CPair1 x v@(CValue2 y)) => do
            putStrLn (show x)
            putStrLn "Value: "
            putStrLn y
        Nothing => putStrLn "Invalid input"
            -}
    putStrLn "��������"
{-     Right f2 <- openFile "input.txt" Read
        | Left err => printLn err
   Right lp <- readDic f1
| Left err => printLn err 
    Right r2 <- fRead f2
        | Left err => printLn err -}
{-    case eval r2 of 
        Just xs => do
            putStrLn xs
            Nothing => putStrLn "Invalid input" -}
--    putStrLn . unwords $ map (\x => getWord x lp) (words "Idris defines several primitive types: Int, Integer and Double for numeric operations, Char and String for text manipulation, and Ptr which represents foreign pointers. There are also several data types declared in the library, including Bool, with values True and False. We can declare some constants with these types. Enter the following into a file Prims.idr and load it into the Idris interactive environment by typing idris2 Prims.idr:")
    printLn . the Double $ 0.1 + 0.2
    putStrLn "\1071\n\1072"

    -- printLn $ bad ()
{-    printLn $ isNonNegative 3.6
    printLn $ doubleEq [1.5, 1.5]
    printLn $ doubleMax [1.5, 1.5, 2.5]
    printLn $ 1.5 == 1.5
    
    let out = decodeDouble 4.5
    printLn $ out.mantissa
-}
-- printLn "m=\{out.mantissa} e=\{out.exponent}" -- s\{out.sign}
